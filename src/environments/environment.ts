// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCUQwhi48j0LpF3O1nMwqNqN35RiMPC_1M',
    authDomain: 'controle-rafael.firebaseapp.com',
    projectId: 'controle-rafael',
    storageBucket: 'controle-rafael.appspot.com',
    messagingSenderId: '530244489025',
    appId: '1:530244489025:web:dc90cbdd0da3ec2d209bc0',
    measurementId: 'G-WXD4YKDCPL'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
